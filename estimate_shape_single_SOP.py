import numpy as np
from scipy.optimize import lsq_linear
import lsqlin

def estimate_shape_single_SOP(shapePC, shapeMU, shapeEV, R, t, s, num_s_d, n_dims, x_2d):
    '''
    Compute optimal shape parameters given R, t, s
    Solve linear system to find best shape parameters to fit 2D landmarks

    Parameters:
        shapePC: 3n by s matrix, n is the number of vertices with s principal components
        shapeMU: 3n by 1 vector containing vertices of mean shape
        shapeEV:
        R
        t
        s
        num_s_d
        dims
        x_2d

    output: 
        b

    NOTE: indexing 3d-array in matlab vs python:
        matlab: a(row, col, page)
        python: a[page, row, col]
    '''
    P = reshape_2_3d_like_matlab(shapePC, int(n_dims), 3, int(shapePC.shape[0] / 3))
    mu = np.reshape(shapeMU, (int(shapeMU.shape[0] / 3), 3)).T
    
    num_of_points = int(P.shape[2])
   
    A = np.zeros((num_of_points * 2, int(n_dims)))
    h = np.zeros(num_of_points * 2)
 
    for j in range(num_of_points):
        A[2 * j] = s * R[0, 0] * np.squeeze(P[:, 0, j])
        A[2 * j] = A[2 * j] + s * R[0, 1] * np.squeeze(P[:, 1, j])
        A[2 * j] = A[2 * j] + s * R[0, 2] * np.squeeze(P[:, 2, j])

        A[2 * j + 1] = s * R[1, 0] * np.squeeze(P[:, 0, j])
        A[2 * j + 1] = A[2 * j + 1] + s * R[1, 1] * np.squeeze(P[:, 1, j])
        A[2 * j + 1] = A[2 * j + 1] + s * R[1, 2] * np.squeeze(P[:, 2, j])

        h[2 * j] = x_2d[0, j] - s * ( R[0, 0] * mu[0, j] + R[0, 1] * mu[1, j] + R[0, 2] * mu[2, j] + t[0] )
        h[2 * j + 1] = x_2d[1, j] - s * ( R[1, 0] * mu[0, j] + R[1, 1] * mu[1, j] + R[1, 2] * mu[2, j] + t[1] )

    c = np.append(np.identity(int(n_dims)), -np.identity(int(n_dims)))
    C = np.reshape(c,[int(n_dims) * 2, int(n_dims)])
    
    d = np.append(num_s_d *shapeEV, num_s_d *shapeEV)

    ## optimization equation: 0.5 * || Ax - h ||^2

    ## This solution is no constrains
    #b = lsq_linear(A, h.T, bounds = (lb-1, lb))

    ## solution with constrain: Cx <= d
    b = lsqlin.lsqlin(A, h, 0, C, d)

    return np.array((b['x']))

def reshape_2_3d_like_matlab(data, num_page, num_row, num_col):
    temp_3d = np.reshape(data.T, (num_page, num_col, num_row))
    data_3d = np.zeros((num_page, num_row, num_col))

    for i in range(num_page):
        data_3d[i, :, :] = temp_3d[i, :, :].T

    return data_3d

# import scipy.io as sio
# test_data = sio.loadmat('estimateShapeSingleSOP_validate_data/EstimateShapeSingleSOP_validate_data.mat')
# x_2d = test_data['xp']
# shapeEV = test_data['shapeEV']
# shapeMU = test_data['shapeMU']
# shapePC = test_data['shapePC']
# R = test_data['R']
# t = test_data['t']
# s = test_data['s']
# num_s_d = test_data['numsd']
# n_dims = test_data['ndims']

# a=np.array([[ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],[25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48]])
# print (a)
# a1 = a.T
# b = reshape_2_3d_like_matlab(a, 8, 3 ,2)
# print (b)

# b = estimate_shape_single_SOP(shapePC, shapeMU, shapeEV, R, t, s, num_s_d, n_dims, x_2d)
# print(b)