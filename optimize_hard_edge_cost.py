import numpy as np
import matrix_operation as mo
import occlude_boundary_vertices as obv
from sklearn.neighbors import NearestNeighbors as knn
from scipy.optimize import least_squares

def optimize_hard_edge_cost(b0, x_landmarks, shapeEV, shapeMU, shapePC, R0, t0, s0, r, c, landmarks, options, faces, run_2_convergence):
    num_s_d = 3
    n_dims = len(b0)
    n_edges = len(options['occluding_vertices'])
    n_landmarks = len(landmarks)

    # define jacobian matrix for least square optimize
    J = np.zeros([2 * n_edges + 2 * n_landmarks + n_dims, 6 + n_dims])
    J[0 : 2 * n_edges + 2 * n_landmarks, : ] = 1
    J[ 2 * n_edges + 2 * n_landmarks : , 6 : ] = np.identity(n_dims)

    ## rescale shape parameters to be units of standard deviation
    b0 = b0 / shapeEV[0 : n_dims]
    b0 = np.append(mo.R_t_s_2_vec(R0, t0, s0), b0)
    # print(b0)
    # print(b0.shape)
    # b0 = np.reshape(b0, [len(b0), 1])
    # print(b0.shape)

    ## define lower bound
    LB = np.empty((1, 5))
    LB[:] = -np.inf
    LB = np.append(LB, 0)
    LB = np.append(LB, -(num_s_d * np.ones([1, n_dims])))

    ## define upper bound
    UB = np.empty((1, 6))
    UB[:] = np.inf
    UB = np.append(UB, num_s_d * np.ones([1, n_dims]))

    if run_2_convergence:
        solution = least_squares(get_hard_edge_cost, 
                                 b0, 
                                 bounds = (LB, UB), 
                                 ftol=1e-09, 
                                 xtol=1e-09, 
                                 args = ( x_landmarks, shapeEV[ : n_dims], shapeMU, shapePC[: , : n_dims], r, c,landmarks, options, faces))
    else:
        solution = least_squares(get_hard_edge_cost, 
                                 b0, 
                                 bounds = (LB, UB), 
                                 ftol=1e-09, 
                                 xtol=1e-09, 
                                 args = ( x_landmarks, shapeEV[ : n_dims], shapeMU, shapePC[: , : n_dims], r, c,landmarks, options, faces),  jac_sparsity = J)

    b = solution['x']

    R, t, s = mo.vec_2_R_t_s(b[0 : 6])
    b_ret = b[6 : ]
    b_ret = b_ret * shapeEV[0 : n_dims].T

    return b_ret.T, R, t, s





def get_hard_edge_cost(b, x_landmarks, shapeEV, shapeMU, shapePC, r, c,landmarks, options, faces):
    '''

    '''
    R, t, s = mo.vec_2_R_t_s(b[0 : 6])
    b = b[6 : ]

    b = np.reshape(b, [len(b), 1])
    b = b * (shapeEV[0 : len(b)])

    X = np.reshape(shapePC.dot(b) + shapeMU, [3, int(shapePC.shape[0] / 3)], order = 'F')
    
    if 'occluding_vertices' not in options:
        '''
        If the occluding_vertices are not found in option_dict,
        we would need to recompute at each iteration (slow and objective continuous)
        '''
        FV = dict()
        FV['faces'] = faces
        FV['vertices'] = X.T
        options['occluding_vertices'] = obv.occlude_boundary_vertices(FV, options['Ef'], options['Ev'], R)
    
    ## scale weights to be invariant to number of landmarks/edges vertices/numbers
    n_edges = len(options['occluding_vertices'])
    n_landmarks = len(landmarks)
    w3 = 1 - options['w1'] - options['w2']
    w1 = options['w1'] / (2 * n_edges)
    w2 = options['w2'] / (2 * n_landmarks)
    w3 = w3 / (len(shapeEV))

    ## compute positions of projected occluding bounding vertices
    idx = options['occluding_vertices']
    # print('\n\n Shape of idx is ')
    # print(idx.shape)
    idx = idx.flatten()

    x_edge = R.dot(X[:, idx])
    x_edge = x_edge[0 : 2, :]
    x_edge[0, :] = s * (x_edge[0, :] + t[0])
    x_edge[1, :] = s * (x_edge[1, :] + t[1])
    
    ## Find edge correspondences
    ## NOTE: Be aware of the differences between r and c in python and matlab
    x = np.reshape(np.append(c, r), [len(c), 2], order = 'F')
    knbrs = knn(n_neighbors = 1, algorithm = 'kd_tree').fit(x)
    distances, indices = knbrs.kneighbors(x_edge.T)

    edge_residuals = np.append(c[indices[:, 0]].T - x_edge[0, :],
                                r[indices[:, 0]].T - x_edge[1, :])

    ## compute position of projected landmark vertices
    x_landmarks_2 = R.dot(X[:, landmarks[:, 0]])
    x_landmarks_2 = x_landmarks_2[0 : 2, :]
    x_landmarks_2[0, :] = s * ( x_landmarks_2[0, :] +t[0] )
    x_landmarks_2[1, :] = s * ( x_landmarks_2[1, :] +t[1] )

    landmark_residuals = np.append(x_landmarks_2[0, :] - x_landmarks[0, :],
                                  x_landmarks_2[1, :] - x_landmarks[1, :])
    
    prior_residuals = b / shapeEV[0 : len(b)]

    residuals = np.append(w1 * edge_residuals, w2 * landmark_residuals)
    residuals = np.append(residuals, w3 * prior_residuals)

    return residuals


# import scipy.io as sio
# import scipy.spatial.distance as dist
# test_data = sio.loadmat('hard_edge_cost/optimiseHardEdgeCost_data.mat')
# result_data = sio.loadmat('hard_edge_cost/optimiseHardEdgeCost_result.mat')
# run_2_convergence = test_data['runtoconvergence']
# # test_data = sio.loadmat('hard_edge_cost/hardEdgeCost_data_1.mat')
# # result_data = sio.loadmat('hard_edge_cost/hardEdgeCost_result_1.mat')
# # print(test_data)
# b0 = test_data['b0']
# R0 = test_data['R0']
# t0 = test_data['t0']
# s0 = test_data['s0']

# x_landmarks = test_data['x_landmarks']
# shapeEV = test_data['shapeEV']
# shapeMU = test_data['shapeMU']
# shapePC = test_data['shapePC']
# r = test_data['r']
# c = test_data['c']
# landmarks = test_data['landmarks'] - 1
# options = test_data['options']
# faces = test_data['faces'] - 1
# options_dict = dict()
# options_dict['Ef'] = options[0]['Ef'][0] - 1
# options_dict['Ev'] = options[0]['Ev'][0] - 1
# options_dict['w1'] = options[0]['w1'][0][0][0]
# options_dict['w2'] = options[0]['w2'][0][0][0]
# options_dict['occluding_vertices'] = options[0]['occludingVertices'][0] - 1
# r = get_hard_edge_cost(b0, x_landmarks, shapeEV, shapeMU, shapePC, r, c,landmarks, options_dict, faces)
# l2_dist = dist.euclidean(r, result_data['residuals'])
# print(r)
# print(result_data['residuals'])
# print(l2_dist)

# b_ret, R, t, s = optimize_hard_edge_cost(b0, x_landmarks, shapeEV, shapeMU, shapePC, R0, t0, s0, r, c, landmarks, options_dict, faces, run_2_convergence)
# print(b_ret)
# print('\n')

# print(R)
# print('\n')

# print(t)
# print('\n')

# print(s)
# # print('\n')

# print(result_data)