### What is this repository for? ###
This is a python implementation for fitting 3DMM using landmark and edge correspondences. 

### Dependencies ###
Developed under python 3.5 environment with following libraries:
	- opencv
	- dlib
	- cvxopt
	- Sklearn 
	- mayavi (optional for render_3d_test)
	- tvtk (optional for render_3d_test)


### Process ###
Under development for more stable system.

![enhance image by opencv]( test_blurry/fitted/group1_2xcvface_0_processed.jpg)
Enhance image by opencv before fitting


![enhance image by opencv]( test_blurry/fitted/group1_ne2xface_0_processed.png)
Enhance image by neural enhancement before fitting
