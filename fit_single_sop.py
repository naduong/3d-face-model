import numpy as np
import estimate_sop_with_refinement as eswr
import estimate_shape_single_SOP as esss
import bundle_adjust_single_SOP as bundle

## NOTE: MatLab index starts from 1 to n (inclusive n)
##       Python index from from 0 to n - 1 (exclusive n) . BE CAREFUL WHEN DEBUGGING BETWEEN MatLab and Python

def fit_single_sop(face_landmarks, shapePC, shapeMU, shapeEV, ndims, landmark_index, w1, w2 = None, nlandmarks = None):
    '''
    Fit a morphable model to observed 2D positions (face_landmarks)
    
    The objective of fitting a morphable model to these observed points is to find
        the shape and pose parameters that minimize the reprojection error between observed
        points and the predicted 2D points.

    Parameters:

    Return:
    '''

    ## check face_landmarks input

    ## number of standard deviations for hyperbox constrains
    num_s_d = 3

    ## number of iterations of coordinate ascent
    num_iter = 10

    ## number of vertices
    num_verts = int(shapePC.shape[0] / 3)

    ## subselect landmark vertices from mean and principal component vectors
    vertex_indices = np.empty((0))
    for i in range(num_verts * 3):
        vertex_indices = np.append(vertex_indices, i)

    sorted_vertices = np.reshape(vertex_indices, [num_verts, 3])

    face_p_s = sorted_vertices[landmark_index.flatten(), 0 : 3]
    sorted_vertices = face_p_s.astype(int)
    sorted_vertices = sorted_vertices.flatten()

    shapePC = shapePC[sorted_vertices, 0 : ndims]
    shapeMU = shapeMU[sorted_vertices, :]
    shapeEV = shapeEV[ : ndims]

    ## Obtain intial estimate of pose parameters using mean shape
    print('\n[PROCESSING]: Initializing camera parameters...')
    x = np.reshape(shapeMU, [3, int(shapePC.shape[0] / 3)], order = 'F')
    R, t, s = eswr.estimate_SOP_with_refinement(face_landmarks, x)

    ## Retrieving initial shape estimate by solving linear system
    print('\n[PROCESSING]: Initializing shape parameters...')
    b = esss.estimate_shape_single_SOP(shapePC, 
                                       shapeMU, 
                                       shapeEV, 
                                       R, t, s, 
                                       num_s_d, 
                                       ndims, 
                                       face_landmarks)
    
    print('\n[PROCESSING]: Coordinate ascent ...')

    ## Perform coordinate ascent, alternatively solving for pose and shape
    ## while fixing the other.
    for i in range(num_iter):
        print('\n\tIteration: ', (i + 1))

        x = np.reshape(shapePC.dot(b) + shapeMU, [3, int(shapePC.shape[0] / 3)], order = 'F')

        R, t, s = eswr.estimate_SOP_with_refinement(face_landmarks, x)
        b = esss.estimate_shape_single_SOP(shapePC, 
                                           shapeMU, 
                                           shapeEV, 
                                           R, t, s, 
                                           num_s_d, 
                                           ndims, 
                                           face_landmarks)

    
    print('\n[PROCESSING]: Performing bundle adjustment ...')

    ## perform non-linear bundle adjustment, simultaneously optimising 
    ## shape and pose parameters
    if w2 is None and nlandmarks is None:
        b, R, t, s = bundle.bundle_adjust_single_SOP(face_landmarks, 
                                                    b, R, t, s, 
                                                    shapePC, 
                                                    shapeMU, 
                                                    shapeEV, 
                                                    num_s_d, w1)
    else:
        b, R, t, s = bundle.bundle_adjust_single_SOP(face_landmarks, 
                                                     b, R, t, s, 
                                                     shapePC, 
                                                     shapeMU, 
                                                     shapeEV, 
                                                     num_s_d, 
                                                     w1, w2, nlandmarks)
    b = np.reshape(b, [len(b), 1])
    return b, R, t, s

# import scipy.io as sio
# test_data = sio.loadmat('FitSingleSOP/FitSingleSOP_data.mat')

# face_landmarks = test_data['xp']
# shapePC = test_data['shapePC']
# shapeMU = test_data['shapeMU']
# shapeEV = test_data['shapeEV']
# ndims = test_data['ndims']
# ## -1 is to adjust matlab index to python index
# landmark_index = test_data['landmarks'] - 1
# w1 = test_data['w1']

# print(face_landmarks.dtype)
# print(landmark_index.dtype)
# print(shapeEV.dtype)
# print(shapePC.dtype)
# print(shapeMU.dtype)
# print(int(ndims[0][0]))

# b, R, t, s = fit_single_sop(face_landmarks, shapePC, shapeMU, shapeEV, int(ndims[0][0]), landmark_index.astype(int), w1)
# print(b)
# print(R)
# print(t)
# print(s)