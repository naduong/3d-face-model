import scipy.io as sio
import dlib
import face_detector as fd
import cv2
import time
import numpy as np
import fit_single_sop as fss
import face_rendering as fr
import fit_edge as fe
import helper
import optimize_hard_edge_cost as ohec
import os, imghdr
import argparse
import render_3d_test as renderView


def normalize_matrix(array):
    maxVal = np.amax(array)
    minVal = np.amin(array)

    for r_i in range(0, len(array)):
            array[r_i] = ((array[r_i] - minVal) * (255) / (maxVal - minVal))
    return np.float64(array)


def convert_2_BFM(landmarks, image):
    '''
    Convert detected landmarks into references into BFM and 2D positions
    in the format used within this implementation. 
    '''
    converted_index_table = [33, 32, 31, 34, 35, 30, 29, 28, 27, 39, 40, 41, 38, 37, 36, 17, 18, 19, 20, 21, 42, 47, 46, 43, 44, 45, 26, 25, 24, 23, 22, 51, 50, 49, 48, 60, 61, 62, 52, 53, 54, 64, 63, 55, 65, 56, 66, 58, 67, 59, 57]
    landmark_id = np.array([8333,7301,6011,9365,10655,8320,8311,8286,8275,5959,4675,3642,4922,
                   3631,2088,27391,39839,40091,40351,6713,10603,11641,12673,11244,
                   12661,14472,27778,41804,41578,41310,9806,8345,7442,6936,5392,
                   7335,7851,8354,9248,9398,11326,9399,9129,9406,9128,8890,8367,7858,7580,7471,8374,23002,32033]) - 1
    landmark_id = np.reshape(landmark_id, [53, 1])
    n_landmarks = np.zeros([53, 2])

    for i in range(len(converted_index_table)):   
        n_landmarks[i, :] = landmarks[converted_index_table[i], :]

    n_landmarks[51] = landmarks[4]
    n_landmarks[52] = landmarks[12]
    n_landmarks[:, 1] = image.shape[0] + 1 - n_landmarks[:, 1]

    return n_landmarks.T, np.uint16(landmark_id)
    
def get_transform_model(shapePC, shapeMU, b, n_dims):
    a = shapePC[:, 0 : n_dims]
    vertices = a.dot(b) + shapeMU
    return np.reshape(vertices, [3, int(shapePC.shape[0] / 3)], order = 'F').T


def fit(imPath, savePath, imName, morphable_model, BFM_edges):
    name, extension = os.path.splitext(imName)
    
    im_path = os.path.join(imPath, imName)

    image = cv2.imread(im_path)
    if (image is None):
        print('Cannot read image ', im_path)
        return None

    Ef = BFM_edges['Ef'] - 1
    Ev = BFM_edges['Ev'] - 1

    ## 3n by 1 vector containing vertices of mean shape
    shapeMU = np.float64(morphable_model['shapeMU'])

    ## 3n by s matrix, n is the number of vertices with s principal components
    shapePC = np.float64(morphable_model['shapePC'])

    ## k by 1 vector, contains the sorted standard deviations f each
    # principal components
    shapeEV = np.float64(morphable_model['shapeEV'])

    ## 3n by 1 vector containing vertices of mean shape
    texMU = np.float64(morphable_model['texMU'])
    texMU = np.reshape(texMU, [int(len(texMU) / 3), 3])

    ## 3n by s matrix, n is the number of vertices with s principal components
    texPC = np.float64(morphable_model['texPC'])

    ## k by 1 vector, contains the sorted standard deviations f each
    # principal components
    texEV = np.float64(morphable_model['texEV'])

    tl = morphable_model['tl']

    # number of model dimensions to use
    n_dims = 40

    # Prior weight for initial landmark fitting
    w_intial_prior = 0.7

    # number iterations for iterative closest fitting
    i_c_f_iter = 7

    ## Initiate landmark predictor and reuse them as global objects
    ## detector is for detecting face
    ## predictor is for landmark prediction
    detector, predictor = fd.initiateDetector()

    ## Use face detection to detect landmarks in all faces available
    ## in that image
    start_time = time.time()

    # Note: landmarks of all faces in images will be detected.
    # For now, just get the first landmarks set
    landmarks = fd.detectFaces(image, detector, predictor)
    if len(landmarks) == 0:
        print ('CANNOT FIND A FACE IN IMAGE...')
    landmarks = landmarks[0]

    detecting_time = time.time() - start_time
    print('\nLandmark detection time = ' , detecting_time)

    # n_landmarks is landmark set of face in image
    # landmark_index is landmark reference from model
    n_landmarks, landmark_index = convert_2_BFM(landmarks, image)

    '''
    Initialize fitting using only landmarks
    '''
    print('Fitting to landmarks only...')
    b, R, t, s = fss.fit_single_sop(n_landmarks, shapePC, shapeMU, shapeEV, n_dims, landmark_index - 1, w_intial_prior)
    # FV = dict()
    # FV['vertices'] = get_transform_model(shapePC, shapeMU, b, n_dims)
    # FV['faces'] = tl - 1
    # # this is for viewing fitting model
    # rendered_image, render_model = fr.render_face(FV, image, R, t, s, False)

    tri = tl - 1
    w_edges = 0.45
    w_landmarks = 0.15
    print('\nFitting to edges with iterative closest edge fitting...')
    b, R, t, s = fe.fit_edges(image, n_landmarks, landmark_index - 1, 
                shapePC, shapeMU, shapeEV, Ef, Ev, tri, 
                n_dims, w_intial_prior, w_edges, w_landmarks, i_c_f_iter)

    FV = dict()
    # FV['vertices'] = get_transform_model(shapePC, shapeMU, b, n_dims)
    FV['faces'] = tl - 1

    # ## this is for viewing fitting model
    # rendered_image = fr.render_face(FV, image, R, t, s, False)
    # cv2.imshow('fit SOP', rendered_image)
    # cv2.waitKey()

    options = dict()
    options['Ef'] = Ef
    options['Ev'] = Ev
    options['w1'] = w_edges
    options['w2'] = w_landmarks

    # Run final optimization of hard edge cost
    print('\nOptimizing non-convex edge cost...')
    max_iter = 5
    cur_iter = 0
    diff = 1
    eps = 1e-09

    # get the information of canny edge of the face image
    c, r = helper.get_edges_info(image)

    while(cur_iter < max_iter) and (diff > eps):
        vertices = get_transform_model(shapePC, shapeMU, b, n_dims)
        FV['vertices'] = vertices

        occluding_vertices, idx = helper.get_occluding_vertices(FV, Ef, Ev, vertices, R, t, s, r, c, 0.95)

        options['occluding_vertices'] = occluding_vertices

        b0 = b

        b, R, t, s = ohec.optimize_hard_edge_cost(b0, n_landmarks, shapeEV, shapeMU, shapePC, 
                                                    R, t, s, r, c, landmark_index - 1, options, FV['faces'], False)

        diff = np.linalg.norm(b0 - b)
        print('\n\tdiff = ', diff)
        cur_iter += 1

    b, R, t, s = ohec.optimize_hard_edge_cost(b0, n_landmarks, shapeEV, shapeMU, shapePC, 
                                                    R, t, s, r, c, landmark_index - 1, options, FV['faces'], True)

    FV['vertices'] = get_transform_model(shapePC, shapeMU, b, n_dims)

    
    renderView.visual_predicted_shape(FV['vertices'], FV['faces'])

    renderView.visual_predicted_shape(FV['vertices'], FV['faces'], texMU)

    save_2d_image(FV, image, R, t, s, savePath, name, extension)

def save_2d_image(FV, image, R, t, s, savePath, name, extension):
    rendered_image, model = fr.render_face(FV, image, R, t, s, False)
#    dis = np.hstack((rendered_image, model))
    dis = np.hstack((image, rendered_image))

    save_path = os.path.join(savePath, name + '_processed' + extension)

    cv2.imwrite(save_path, dis)
    cv2.imshow('model', dis)
    cv2.waitKey()   



morphable_model = sio.loadmat('BFM/01_MorphableModel.mat')
# Load pre-computed edge-vertex and edge-face list 
BFM_edges = sio.loadmat('BFMedgestruct.mat')

# renderView.visual_mean_model_with_texture()

# get argument in
argParser = argparse.ArgumentParser()
argParser.add_argument("-i", "--input", help = "input can be image file or image folder")
ag = argParser.parse_args()

if os.path.isfile(ag.input):
    fit('', '', ag.input, morphable_model, BFM_edges)

elif os.path.isdir(ag.input):
    newFolderName = 'fitted'
    ## prepare new folder path to save face
    newFolderPath = os.path.join(ag.input, newFolderName)
    if not os.path.exists(newFolderPath):
        os.makedirs(newFolderPath)

    for imName in os.listdir(ag.input):
        fit(ag.input, newFolderPath, imName, morphable_model, BFM_edges)

else:
    print 'Invalid input argument.'