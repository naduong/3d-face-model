import numpy as np
import scipy
import matrix_operation as mo
from scipy.optimize import least_squares
##TODO:
# 1. describe the function
# 2. try to understand it
# 3. fine-tun the optimization with more parameters 

def refine_ortho_cam(x, X, b0):
    '''
    perform non-linear optimization simultaneouly 
    '''
    b = least_squares(ortho_cam_linear_err, b0, args = (x, X))
    return b.x


def ortho_cam_linear_err(b, x, X):
    '''
    '''
    r = b[0 : 3]
    t = b[3 : 5]
    s = b[5]

    r_norm = np.linalg.norm(r)

    # TODO: check if any element in r is complex
    if r_norm == 0:
        R = np.linalg.identity(3)
    else:
        r = r / r_norm
        r1 = np.append(r, r_norm)
        R = mo.aa_rot_vec_2_mat(r1)

    x2 = R.dot(X)
    x2[0, : ] = s * (x2[0 , :] + t[0])
    x2[1, : ] = s * (x2[1 , :] + t[1])

    result = np.append(x[0, :] - x2[0, :], x[1, :] -x2[1, :])

    return result



#####################################################################
############## VALIDDATE FUNCION ####################################
# import scipy.io as sio
# b_data = sio.loadmat('OrthoCamNonlinerrfun_validate_data/b.mat')
# b = b_data['b'].T
# print (b)

# x_data = sio.loadmat('OrthoCamNonlinerrfun_validate_data/x.mat')
# x = x_data['x']
# print (x)

# X_data = sio.loadmat('OrthoCamNonlinerrfun_validate_data/x_cap.mat')
# X = X_data['X']
# print (X)
# r = ortho_cam_linear_err(b[0],x,X)
# print (r)
# ## NOTE: validate r with OrthoCamNonlinerrfun_validate_data/result.mat

# b_r = refine_ortho_cam(x, X, b[0])
# print (b_r)
# ## NOTE: validate b_r with refine_ortho_cam_validate_data/b_result.mat


