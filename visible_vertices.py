import numpy as np

def get_visible_vertices(FV, R):
    '''
    '''
    
    V2 = (R.dot(FV['vertices'].T)).T
    ## Store the vertex depth into z_buffering
    Z = V2[:, 2]

    ## get the projected vertices onto the image plane
    UV = np.zeros([V2.shape[0], 2])
    
    ## orthographic projection for x
    UV[:, 0] = V2[:, 0]

    ## orthographic projection for y
    UV[:, 1] = V2[:, 1]
    
    ## transform to the pixel plane (the axis remains switched)
    UV[:, 0] = UV[:, 0] - np.min(UV[:, 0])
    UV[:, 1] = UV[:, 1] - np.min(UV[:, 1])
    UV = UV + 1

    UV = UV / np.max(UV[:]) * 1000
    width = 1000
    height = 1000

    ## get the triangle vertices
    faces = FV['faces']
    v1 = faces[:, 0]
    v2 = faces[:, 1]
    v3 = faces[:, 2]
    numb_faces  = faces.shape[0]

    ## compute bounding boxes for the projected triangles
    x = np.zeros([len(v1), 3])
    x[:, 0] = UV[v1, 0]
    x[:, 1] = UV[v2, 0]
    x[:, 2] = UV[v3, 0]

    y = np.zeros([len(v1), 3])
    y[:, 0] = UV[v1, 1]
    y[:, 1] = UV[v2, 1]
    y[:, 2] = UV[v3, 1]
    
    minx = np.ceil(np.amin(x, axis = 1))
    maxx = np.floor(np.max(x, axis = 1))
    miny = np.ceil(np.min(y, axis = 1))
    maxy = np.floor(np.max(y, axis = 1))

    ## frustum culling ???
    minx = np.maximum([1], minx).astype(int)
    maxx = np.minimum([width], maxx).astype(int)
    miny = np.maximum([1], miny).astype(int)
    maxy = np.minimum([height], maxy).astype(int)
 
    ## construct the pixel grid -- can recompute if shared among images
    x_space = np.linspace(0, width - 1, width, dtype = 'int')
    y_space = np.linspace(0, height - 1, height, dtype = 'int')
    rows, cols = np.meshgrid(x_space, y_space)

    ## create buffers for: depth(z), face(f), and weights
    z_buffer = np.full([height, width], -np.inf)
    f_buffer = np.zeros([height, width])
    w1_buffer = np.full([height, width], np.nan)
    w2_buffer = np.full([height, width], np.nan)
    w3_buffer = np.full([height, width], np.nan)

    ## for each triangle (can speed up by comparing the triangle depths to the z-buffer
    ## and priorly sorting the triangles by increasing depths)
    i = 1
    for i in range(numb_faces):
        ## if some pixels lie in the bounding box
        if minx[i] <= maxx[i] and miny[i] <= maxy[i]:
            ## get the pixels lying in bounding box
            ## Note that + 1 here is to adjust indexing from matlab to python
            px = rows[miny[i] : maxy[i] + 1, minx[i] : maxx[i] + 1]
            py = cols[miny[i] : maxy[i] + 1, minx[i] : maxx[i] + 1]

            px = px[:]
            py = py[:]
            
            e0 = UV[v1[i], :]
            e1 = UV[v2[i], :] - e0
            e2 = UV[v3[i], :] - e0

            ## compute the barycentric coordinates (can speed up by first computing and testing a sorely)
            det = e1[0] * e2[1] - e1[1] * e2[0]

            tmpx = px - e0[0]
            tmpy = py - e0[1]
            a = (tmpx * e2[1] - tmpy * e2[0]) / det
            b = (tmpy * e1[0] - tmpx * e1[1]) / det

            ## NOTE: there should be an easier way to write this
            test = np.ones(a.shape, dtype = 'bool')
            for k in range(a.shape[0]):
                for l in range(a.shape[1]):
                    test[k, l] = (a[k, l] >= 0 and b[k, l] >= 0 and (a[k, l] + b[k, l]) <= 1)

            if test.any():
                ## get the pixels on inside the triangle
                px = px[test]
                py = py[test]

                ## interpolate the triangle for each pixel
                w2 = a[test]
                w3 = b[test]
                w1 = 1 - w2 - w3
               
                pz = Z[v1[i]] * w1 + Z[v2[i]] * w2 + Z[v3[i]] * w3

                for j in range(len(pz)):
                    if pz[j] > z_buffer[py[j], px[j]]:
                        z_buffer[py[j], px[j]] = pz[j]
                        f_buffer[py[j], px[j]] = i
                        w1_buffer[py[j], px[j]] = w1[j]
                        w2_buffer[py[j], px[j]] = w2[j]
                        w3_buffer[py[j], px[j]] = w3[j]


    ## NOTE: and again, there should be an easy way to handle this
    test = np.ones(f_buffer.shape, dtype = 'bool')
    for i in range(f_buffer.shape[0]):
        for j in range(f_buffer.shape[1]):
            test[i, j] = (f_buffer[i, j] != 0)

    f = np.unique(f_buffer[test]).astype(int)
    vv = np.zeros([len(f), 3])
    vv[:, 0] = v1[f]
    vv[:, 1] = v2[f]
    vv[:, 2] = v3[f]
    v = np.unique(vv)
    
    ## from MatLab
    #f       = find(any(ismember(FV.faces, v), 2));
    #Nfaces  = length(f);

    return v.astype(int)


# import scipy.io as sio
# test_data = sio.loadmat('visible_vertices/visiblevertices_data2.mat')
# FV = test_data['FV']
# R = test_data['R']
# ## redefine FV as dict, contains 2 keys: "vertices" and "faces"
# new_FV = dict()
# ## -1 is to adjust matlab index into python index
# new_FV['faces'] = FV[0,0][0] - 1
# new_FV['vertices'] = FV[0,0][1]
# v = get_visible_vertices(new_FV, R)
# result_data = sio.loadmat('visible_vertices/visiblevertices_result2.mat')
# result = result_data['visible']
# print(result.shape)
# print(v.shape)
# count = 0
# for i in range(len(v)):
#     if v[i] != result[i] - 1:
#         count = count + 1
# print (count)