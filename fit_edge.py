import numpy as np
import cv2
import fit_single_sop as fss
import occlude_boundary_vertices as obv
from sklearn.neighbors import NearestNeighbors as knn
import helper

def fit_edges(image, landmarks_2d, landmarks_3d_index, 
            shapePC, shapeMU, shapeEV, Ef, Ev, tri, 
            n_dims, w_prior, w_edges, w_landmarks, niter):

    FV = dict()

    ## Recall tri is face structure of 3DMM mesh
    FV['faces'] = tri

    percentile = 1

    ## perform initial landmark-only fit
    ## Recall that b is estimated shape parameters
    ## R, t, s are estimated rotation matrix, translation and scale
    b, R, t, s = fss.fit_single_sop(landmarks_2d, 
                                    shapePC, shapeMU, shapeEV, 
                                    n_dims, landmarks_3d_index, w_prior)

    
    vertices_1D = np.dot(shapePC[:, 0 : n_dims], b) + shapeMU
    vertices = np.reshape(vertices_1D, 
                          [3, int(shapePC.shape[0] / 3)], 
                          order = 'F').T

    FV['vertices'] = vertices
    
    # get the information of canny edge of the face image
    c, r = helper.get_edges_info(image)

    for i in range(niter):
        print('\n\niteration = ', i)
        occluding_vertices, idx = helper.get_occluding_vertices(FV, Ef, Ev, vertices, R, t, s, r, c, percentile)
           
        x = np.append(c[idx], landmarks_2d[0, :])
        y = np.append(r[idx], landmarks_2d[1, :])
        xp = np.append([x],[y], axis = 0)

        ## DO WE NEED TO ADJUST INDICES OF LANDMARKS_3D_INDEX?????
        landmark_index = np.append(occluding_vertices, landmarks_3d_index)

        b, R, t, s = fss.fit_single_sop(xp, 
                                        shapePC, shapeMU, shapeEV, 
                                        n_dims, landmark_index,
                                        w_edges, w_landmarks, landmarks_3d_index.shape[0])

        vertices_1D = np.dot(shapePC[:, 0 : n_dims], b) + shapeMU
        vertices = np.reshape(vertices_1D, 
                          [3, int(shapePC.shape[0] / 3)], 
                          order = 'F').T
        FV['vertices'] = vertices

        
        # [b,R,t,s] = FitSingleSOP( [c(idx)' xp(1,:); r(idx)' xp(2,:)],shapePC,shapeMU,shapeEV,ndims,[occludingVertices; landmarks],w_edges,w_landmarks,length(landmarks));
        # % Note: this is completely refitting from scratch. We could just
        # % re-start the nonlinear optimisation using previous estimates as
        # % initialisation but this doesn't seem to work well.
        # %disp(num2str(norm(b-bold)));
        # %differb(iter)=norm(b-bold);
        # FV.vertices = reshape(shapePC(:,1:ndims)*b+shapeMU,3,size(shapePC,1)/3)';
        # %bold=b;

    return b, R, t, s



# import scipy.io as sio
# test_data = sio.loadmat('fit_edges_data/FitEdges_data.mat')
# image = test_data['im'] 
# landmarks_2d = test_data['xp']
# landmarks_3d_index = test_data['landmarks'] 
# shapePC = test_data['shapePC']
# shapeMU = test_data['shapeMU'] 
# shapeEV = test_data['shapeEV']
# Ef = test_data['Ef'] - 1
# Ev = test_data['Ev'] - 1
# tri = test_data['tri'] - 1
# n_dims = test_data['ndims']
# w_prior = test_data['w_prior']
# w_edges = test_data['w_edges']
# w_landmarks = test_data['w_landmarks']
# niter = int(test_data['niter'])    

# fit_edges(image, landmarks_2d, landmarks_3d_index, 
#             shapePC, shapeMU, shapeEV, Ef, Ev, tri, 
#             int(n_dims), w_prior, w_edges, w_landmarks, niter)