import numpy as np

def normalize_face(V, F):
    '''
    Compute face normal of triangular mesh
    
    Parameters:
        V:
        F:

    Return:
        face_normals:
    '''
    v1 = F[:, 0]
    v2 = F[:, 1]
    v3 = F[:, 2]

    ## Compute the edge vectors
    e1s = V[v2, :] - V[v1, :]
    e2s = V[v3, :] - V[v1, :]

    ## get the cross product between edge vectors
    fn = np.cross(e1s, e2s)
    fn_norm = np.sqrt(fn[:, 0]**2 + fn[:,1]**2 + fn[:,2]**2)

    ## replicate fn_norm into 3 columns
    norm_repmat = np.reshape(np.tile(fn_norm, [1, 3]), [len(fn_norm), 3], order = 'F')
    fn = fn / norm_repmat
    
    return fn


import scipy.io as sio
test_data = sio.loadmat('facenormals/facenormals_data.mat')
F = test_data['F'] - 1
v = test_data['V']
normalize_face(v,F)
