import numpy as np
import render_face_back as rfb
import render_face_FV as rff
from scipy.interpolate import RectBivariateSpline
from scipy.interpolate import interp2d
from scipy.interpolate import RegularGridInterpolator
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import cv2


def render_face(FV, im, R, t, s, is_texture):
    '''
    Orthographic projection of the mesh onto the image

    Parameters:
        FV:
        im:
        R, t, s:
        is_texture:

    Return:

    '''
    # rendering parameters
    oglp = dict()
    oglp['height'] = im.shape[0]
    oglp['width'] = im.shape[1]
    oglp['i_amb_light'] = np.array([1, 1, 1])
    oglp['i_dir_light'] = np.array([1, 1, 1])

    Rr = np.identity(4)
    Rr[0 : 3, 0 : 3] = R
    Sr = np.identity(4) * s
    Tr = np.identity(4)
    Tr[0 : 2, 3] = t
    T = Tr.dot(Sr)
    T = T.dot(Rr)

    if is_texture:
        ## get face texture
        oglp['i_dir_light'] = np.array([0, 0, 0])
        render_image = rfb.render_face_back(FV, T, oglp, im)
        render_model = None
    else:
        render_image, render_model = rff.render_face_fv(FV, T, oglp, im)
     
    return render_image, render_model


def get_texture(FV, R, t, s, image):
    '''
    TODO: REVISIT and COMPLETE this function
    '''
    t = t.flatten()
    vertices = FV['vertices']
    rot_points = R.dot(vertices.T)
    
    points_2D = np.vstack(( s * (rot_points[0, :] + t[0]), s * (rot_points[1, :] + t[1]) ))

    height, width, depth = image.shape

    ## construct the pixel grid -- can recompute if shared among images
    x_space = np.linspace(1, width + 1, width, dtype = 'int')
    y_space = np.linspace(1, height + 1, height, dtype = 'int')
    # print(x_space)
    # print(y_space)
    xx = points_2D[0, :]

    rows, cols = np.meshgrid(x_space, y_space)
    data = image[:, :, 0]

    aaaa = points_2D[0, :]
    bbbb = height + 1 - points_2D[1, :]
    pts = np.array((aaaa, bbbb))
    print(pts.T)

    ## use RegularGridInterpolator
    my_inter_f = RegularGridInterpolator((x_space, y_space), data.T)
    ccc = my_inter_f(pts.T)
    print("use RegularGridInterpolator")
    print(ccc)
    print('\n\n')
    

    ## use grid data
    pts = np.array((rows.ravel(), cols.ravel()))
    print('use griddata')
    grid_z1 = griddata(pts.T, data.ravel(), (aaaa, bbbb), method='linear')
    print(grid_z1)


    ## use interp2d
    x = np.arange(0, width)
    y = np.arange(0, height)
    f = interp2d(x, y, data.ravel(), kind = 'linear')
    interp_result = f(bbbb[0], aaaa[0])
    print(interp_result)

    ## use RectBivariateSpline
    interp_spline = RectBivariateSpline(y_space, x_space, data)
    spline_result = interp_spline(bbbb[0], aaaa[0])
    print(spline_result)












    # face_n_vertex = np.zeros(vertices.shape)
    # aaaa = points_2D[0, :]
    # bbbb = height + 1 - points_2D[1, :]
    # print(points_2D[1, :].shape)
    # # print(typeOf(rot_points))
    # # https://scipython.com/book/chapter-8-scipy/examples/two-dimensional-interpolation-with-scipyinterpolaterectbivariatespline/
    # for i in range(1):
 
        
    #     interp_spline = interp2d(rows, cols, image[:, :, i])
    #     print(interp_spline)
    #     a = interp_spline(aaaa[0], bbbb[0])
    #     print(a)
        



# import scipy.io as sio

# test_data = sio.loadmat('render_face_data.mat')
# FV_m = test_data['FV']
# image = test_data['im']
# R = test_data['R']
# t = test_data['t']
# s = test_data['s']
# ## redefine FV as dict, contains 2 keys: "vertices" and "faces"
# FV = dict()
# # -1 is to adjust matlab index into python index
# FV['vertices'] = FV_m[0,0][0]
# FV['faces'] = FV_m[0,0][1] - 1
# image = image / 255

# # print(image[:, :, 0])
# get_texture(FV, R, t, s, image)