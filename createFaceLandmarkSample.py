import numpy as np
import cv2
import json
from pprint import pprint
import os, glob
import argparse
from face import face_detector as fd
from face import crop_face as cf
from face import rotate_face as rf
from face import crop_parts as cp
import dlib

## Initiate landmark predictor and reuse them as global objects
## detector is for detecting face
## predictor is for landmark prediction
detector, predictor = fd.initiateDetector()

## Distance between two eyes, reference for scaling image
EYE_DISTANCE = 40

def getScaledFacesInImage(image):
    ###########################################################################
    #
    #
    #
    ###########################################################################

    ## Use face detection to detect landmarks in all faces available
    ## in that image
    landmarksOfAllFaces = fd.detectFaces(image, detector, predictor)

    croppedFaces = list()

    ## Use data of each face to crop and scale that face
    for faceIndex in range(0, len(landmarksOfAllFaces)):
        landmarks = landmarksOfAllFaces[faceIndex]

        ## scale image according to inter eye distance preference
        ## NOTE: this step is NOT OPTIMIZED. It could be done in better way.
        scaledImage, scaledLandmarks = cf.scaleFace(
            image,
            landmarks,
            EYE_DISTANCE)

        ## crop single face
        ## 1 indicate preCropBoudingBox
        ## This is a preCrop step before checking angle and doing rotation
        precroppedFace, shiftedLandmarks = cf.cropFace(
            scaledImage,
            scaledLandmarks, 1)

        ## rotate face based on the reference angle between a line that
        ## connecting 2 eyes and x-axis
        rotatedFace, adjustedLandmarks = rf.rotateFace(precroppedFace, shiftedLandmarks)

        croppedFace, aLandmarks = cf.cropFace(
            rotatedFace,
            adjustedLandmarks, 0)

        cropParts = cp.crop(croppedFace, aLandmarks, [0,1,2,3,4,5,6,7,8,9,10,11])

        croppedFaces.append(cropParts)

    return croppedFaces


def processImage(imageName, newFolderName, folderPath = os.getcwd()):
    ###############################################################################
    #
    # Function to read in image from image path, process image and save that image
    #
    # return code. 0 means failed, 1 means successful.
    #
    ###############################################################################

    ## read image from image name and folder path
    imagePath = os.path.join(folderPath, imageName)
    image = cv2.imread(imagePath)

    if image is None:
        print "Cannot read image from path " + imagePath
        return

    ## prepare new folder path to save face
    newFolderPath = os.path.join(folderPath, newFolderName)
    if not os.path.exists(newFolderPath):
        os.makedirs(newFolderPath)

    imageWithoutExtension, extension = os.path.splitext(imageName)

    print "\nProcessing " + imagePath + "..."

    ## get all cropped faces in an image
    croppedFaces = getScaledFacesInImage(image)

    ## saving all cropped faces
    for faceIndex in range(0, len(croppedFaces)):
        cropParts = croppedFaces[faceIndex]

        for cropName, crop in cropParts.items():
            newFileName = imageWithoutExtension + "_" + str(faceIndex) + "_" + cropName + extension
            cv2.imwrite(os.path.join(newFolderPath, newFileName), crop)


# get argument in
argParser = argparse.ArgumentParser()
argParser.add_argument("-i", "--image", help = "path to input image")
argParser.add_argument("-f", "--folder", help = "path to image folder")
argParser.add_argument("-nf", "--newFolder", help = "result folder name")

ag = argParser.parse_args()

## prepare new folder to save all detected cropped faces
if ag.newFolder is not None:
   newFolder = ag.newFolder
else:
    newFolder = "_detected"

## process a single image if argument is image type
if ag.image is not None:
    path, fileName = os.path.split(ag.image)
    processImage(fileName, newFolder, path)
## if the argument is a folder, process each file in a folder
elif ag.folder is not None:
    for imageName in os.listdir(ag.folder):
        processImage(imageName, newFolder, ag.folder)
else:
    print "No argument. Please try --help to see options."
