import numpy as np

## crop parts code
##   Code       Crop Parts            Composed landmark points
##   -----------------------------------------------
##     0       Full Face
##     1       Two Eyes with Nose
##     2       Two Eyes
##     3       Left Face
##     4       Right Face
##     5       Left Eye with Nose
##     6       Right Eye with Nose
##     7       Left Eye
##     8       Right Eye
##     9       Nose
##    10       Nose and Mouth
##    11       Mouth

cropPartPoints = ([],          ## empty for full face
    [], ##
    [], ##
    [], ##
    [], ##
    [], ##
    [], ##
    [36, 37, 38, 39, 40, 41], ## Left Eye
    [42, 43, 44, 45, 46, 47], ## Right Eye
    [], ##
    [], ##
    [], ##
    []) ##
def crop(croppedImage, shape, cropPartIndices, offset = 2):
    ###########################################################################
    ##
    ##
    ##
    ###########################################################################

    ## TODO: add error handling for croppedImage and cropPartCode

    cropParts = list()
    print cropPartPoints
    height, width, unused = croppedImage.shape
    for cropPartIndex in cropPartIndices:
        print cropPartIndex
        print cropPartPoints[cropPartIndex]
        if cropPartIndex == 0:
            cropParts.append(croppedImage)
        else:
            topLeftX = max(0, min(shape[index][0] for index in cropPartPoints[cropPartIndex]) - offset * 2)
            topLeftY = max(0, min(shape[index][1] for index in cropPartPoints[cropPartIndex]) - offset)

            bottomRightX = min(width, max(shape[index][0] for index in cropPartPoints[cropPartIndex]) + offset * 2)
            bottomRightY = min(height, max(shape[index][1] for index in cropPartPoints[cropPartIndex]) + offset)

            print topLeftX
            print topLeftY

            print bottomRightX
            print bottomRightY

            cropParts.append(croppedImage[topLeftY : bottomRightY, topLeftX : bottomRightX])

    return cropParts

