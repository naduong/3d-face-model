import numpy as np
import POS
import refine_ortho_cam as ror
import matrix_operation as mo

def estimate_SOP_with_refinement(x_2d, x_3d):

    R, t, s = POS.POS(x_2d, x_3d)
    b0 = mo.R_t_s_2_vec(R, t, s)
    b = ror.refine_ortho_cam(x_2d, x_3d, b0)
    R, t, s = mo.vec_2_R_t_s(b)

    return R, t, s

### Test and validate
# import scipy.io as sio
# x_data = sio.loadmat('EstimateSOPWithRefinement_validate_data/x_3d.mat')
# x_3d = x_data['x']


# x1_data = sio.loadmat('EstimateSOPWithRefinement_validate_data/x_2d.mat')
# x_2d = x1_data['xp']

# print (x_3d)
# #print (x_2d)

# R, t, s = estimate_SOP_with_refinement(x_2d, x_3d)
# print (R)
# print (t)