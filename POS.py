import numpy as np

def POS(x_2d, x_3d):
    '''
    This function is to get the pose estimation R, t, using a simple extension of POS algorithm.
    
    POS = Pose from Orthography and Scaling 
    (Read https://pdfs.semanticscholar.org/205b/849cb52e5bcaac0b697027cc191e986d0654.pdf)
    Estimate scaled orthographic projection paramaters from 2D-3D correspondences

    The algorithm is loosely based on the first iteration of the POTIT, enforcing a valid
    rotation matrix via SVD. 

    Parameters:
        x_2d - numpy ndarray: 2 * n matrix of 2D feature point positions
        x_3d - numpy ndarray: 3 * n matrix of 3D feature point positions

    Return:
        R - numpy ndarray: rotation matrix
        s - float: scale
        t - numpy array: translation
    '''

    numb_points = x_2d.shape[1]

    ## build a linear system with 8 unknown of projection matrix
    A = np.zeros((2 * numb_points, 8))

    A[0 : 2 * numb_points - 1 : 2, 0 : 3] = x_3d.T
    A[0 : 2 * numb_points - 1 : 2, 3] = 1

    A[1 : 2 * numb_points : 2, 4 : 7] = x_3d.T
    A[1 : 2 * numb_points : 2, 7] = 1

    b = np.reshape(x_2d.T, (2 * numb_points, 1))

    k = np.linalg.solve(A.T.dot(A), A.T.dot(b))

    ## extract result from recovered vector k
    R1 = k[0 : 3]
    R2 = k[4 : 7]
    sTx = k[3]
    sTy = k[7]

    R1_norm = np.linalg.norm(R1)
    R2_norm = np.linalg.norm(R2)

    s = (R1_norm + R2_norm) / 2

    R = np.zeros((3,3))
    r1 = R1 / R1_norm
    r2 = R2 / R2_norm
    R[0, :] = r1[:, 0]
    R[1, :] = r2[:, 0]

    R[2, :] = np.cross(r1.T, r2.T) 

    ## No need to tranpose V before dot with U. 
    [U, S, V] = np.linalg.svd(R)
    R = U.dot(V)

    if (np.linalg.det(R) < 0):
        U[2, :] = -U[3, :]
        R = U.dot(V)

    t = np.zeros(2)
    t[0] = sTx / s
    t[1] = sTy / s

    return R, t, s


# #####################################################################
# ############## VALIDDATE FUNCION ####################################
# import scipy.io as sio
# x_data = sio.loadmat('POS_validate_data/x.mat')
# x_3d = x_data['x']


# x1_data = sio.loadmat('POS_validate_data/xp.mat')
# x_2d = x1_data['xp']
# R, s, t = POS(x_2d, x_3d)
# print (R)
# print (s)
# print(t)
# ## Validate R, s, t with result from matlab