import numpy as np
import math

'''
http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm
https://stackoverflow.com/questions/32485772/how-do-axis-angle-rotation-vectors-work-and-how-do-they-compare-to-rotation-matr
'''

def R_t_s_2_vec(R, t, s):
    '''
    Convert matrix R, vector t and scalar s to vector representation of pose

    Parameters:

    Returns: 

    '''

    r = aa_rot_mat_2_vec(R)
    r1 = r[0 : 3]
    norm_r1 = np.linalg.norm(r1)

    b = np.zeros(6)
    b[0 : 3] = r1 / norm_r1 * r[3]
    b[3 : 5] = t
    b[5] = s

    return b

def aa_rot_mat_2_vec(mat, epsilon = 1e-12):
    '''
    Convert rotation matrix to axis-angle representation
    
    Parameters:
        mat - numpy ndarray: 3 by 3 matrix
        epsilon - float : minimum value to treat number as zero

    Returns:
        r - numpy array: 3-element vector
    '''
    if mat.shape[0] != 3 or mat.shape[1] != 3:
        raise AttributeError('matrix must be 3 by 3.')

    mat_trace = np.trace(mat)

    if (abs(mat_trace - epsilon) <= epsilon):
        ## phi == 0
        ## use default VRML SFRotation, [0 1 0 0], see VRML97 Standard, C.6.7
        return np.array([0, 1, 0, 0])

    elif (abs(mat_trace + 1) <= epsilon):
        ## phi == pi
        ## This singularity requires elaborate sign ambiguity resolution
  
        ## compute axis of rotation, make sure all elements are >= 0
        ## real signs are obtained by the flipping algorithm below
        b = (np.diag(mat) + 1) /2
        axis = np.zeros(len(b))
        for i in range(len(b)):
            axis[i] = sqrt(max(b[i], 0))
        
        ## axis elements that are <= epsilon set to zero
        axis = axis * (axis > epsilon)

        m_upper = np.array([mat[1,2], mat(0,2), mat(0,1)])

        ## elements with || smaller than epsilon are considered to be 0
        signs = sign(m_upper) * (abs(m_upper) > epsilon);
  
        ## Flipping
        ## The algorithm uses the elements above diagonal to determine the signs 
        ## of rotation axis coordinate in the singular case Phi = pi. 
        ## All valid combinations of 0, positive and negative values lead to 
        ## 3 different cases:
        ## If (Sum(signs)) >= 0 ... leave all coordinates positive
        ## If (Sum(signs)) == -1 and all values are non-zero 
        ##   ... flip the coordinate that is missing in the term that has + sign, 
        ##       e.g. if 2AyAz is positive, flip x
        ## If (Sum(signs)) == -1 and 2 values are zero 
        ##   ... flip the coord next to the one with non-zero value 
        ##   ... ambiguous, we have chosen shift right
  
        ## construct vector [M23 M13 M12] ~ [2AyAz 2AxAz 2AxAy]
        ## (in the order to facilitate flipping):    ^
        ##                                  [no_x  no_y  no_z ]

        if count_x(signs, -1) == 0:
            ## none of the signs is negative
            ## don't flip any axis element
            flip = np.array([1, 1, 1])
    
        elif count_x(signs, 0) == 0 and sum(signs) == -1:
            ## none of the signs is zero, 2 negative 1 positive
            ## flip the coordinate that is missing in the term that has + sign
            flip = -signs;
    
        else:
            ## 2 signs are 0, 1 negative
            ## flip the coord to the right of the one with non-zero value 
            ## [-1 0 0]->[1 -1 1], [0 -1 0]->[1 1 -1], [0 0 -1]->[-1 1 1]
            shifted = np.array([signs[2], signs[0], signs[1]])
            flip = shifted + (shifted == 0)
 
  
        ## flip the axis
        axis = axis * flip;
        r = np.append(axis, math.pi)
    
    else:
        ## General case
        phi = math.acos((mat_trace - 1) / 2)
        den = 2 * math.sin(phi)
        axis = np.array([mat[2,1] - mat[1,2], mat[0,2] - mat[2, 0], mat[1,0] - mat[0,1]]) / den
        r = np.append(axis, phi)

    return r

def vec_2_R_t_s(v):
    '''
    Convert a vector representation of pose to matrix R, vector t and scalar s

    Parameters:
        v - numpy array : a vector representation of pose

    Return:
        R - numpy ndarray : matrix R
        t - numpy array : vector t
        s - float

    ** NOTE: use v = [1,0,1,2,2,1.5] and vertify output from this function with
             matlab function, the result should be the same.

    '''
    
    t = v[3 : 5]
    s = v[5]
    
    ## get norm of r vector to verify r
    r = np.array(v[0 : 3])
    norm_r = np.linalg.norm(r)

    if (norm_r == 0):
        R = np.identity(len(r))
    else:
        r = r / norm_r
        r1 = np.insert(r, 3, norm_r)
        R = aa_rot_vec_2_mat(r1)
    
    return R, t, s


def aa_rot_vec_2_mat(v):
    '''
    Get a matrix representation of rotation defined by axis-angle rotation vector v.

    Parameters:
        v - numpy array: rotation vector, row vector with 4 elements. First three elements are 
        (x,y,z) coordinates, and last element define the angle.

    Return:
        r_mat - numpy ndarray : matrix represention of v

    ** NOTE: has verified output with output from vrrotvec2mat(v) MatLab.
    '''
    if len(v) != 4:
        raise AttributeError('vector v must have length 4.')
    
    ## construct coeffients to create matrix
    s = math.sin(v[3])
    c = math.cos(v[3])
    t = 1 - c
    
    ## unit vector parallel with v
    v1 = normalize_vec (v[0 : 3])

    x, y, z = v1[0], v1[1], v1[2]

    r_mat = np.array([
        [t * x *x + c, t * x * y - s * z, t * x * z + s * y],
        [t *x * y + s * z, t * y * y + c, t * y * z - s * x],
        [t * x * z - s * y, t * y * z + s * x, t * z * z + c]
        ])

    return r_mat

def normalize_vec(v, epsilon = 1e-12):
    '''
    Return a unit vector v_unit parallel to input vector v.

    Parameters:
        v - numpy array : 1 by n vector
        epsilon - float : if norm(v) <= epsilon, return zero vector

    Return:
        v_unit - numpy array : unit vector 
    '''
    
    v_unit = np.zeros(len(v))

    ## get norm of vector v
    norm_v = np.linalg.norm(v)

    if (norm_v > epsilon):
        v_unit = v / norm_v

    return v_unit

def count_x(v, x):
    '''
    Count number of element x in a vector x
    '''
    return np.count_nonzero(v == x)

def reshape_2_3d_like_matlab(data, num_page, num_row, num_col):
    print(data.shape)
    temp_3d = np.reshape(data.T, (num_page, num_col, num_row))
    data_3d = np.zeros((num_page, num_row, num_col))

    for i in range(num_page):
        data_3d[i, :, :] = temp_3d[i, :, :].T

    return data_3d
    
####################################################
## TESTING FUNCTION ################################
# a = [1, 0, 1, 2]
# x = aa_rot_vec_2_mat(a)
# b = aa_rot_mat_2_vec(x)
# print (b)
# #print (x)
# b = [1,0,1,2,2,1.5]
# R,t,s = vec_2_R_t_s(b)
# print (R)
# print(t)
# print (s)

# b1 = R_t_s_2_vec(R, t, s)
# print (b1)
