from imutils import face_utils
import numpy as np
import imutils
import dlib
import cv2
import os
import math

def initiateDetector():
    landmarkPredictorPath = "detectionModel/shape_predictor_68_face_landmarks.dat"
    return dlib.get_frontal_face_detector(), dlib.shape_predictor(landmarkPredictorPath)


def detectFaces(image, detector, predictor):
    ###########################################################################
    #
    # Note: How to handle the case when an image has multiple faces?????
    #
    ###########################################################################
    grayImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect faces using HOG features - dlib
    dlibRects = detector(grayImage, 1)

    landmarksOfAllFaces = list()

    for (i, rect) in enumerate(dlibRects):
        ## predict landmarks using dlib predictor
        shape = predictor(grayImage, rect)

        ## convert to numpy array
        landmarks = face_utils.shape_to_np(shape)

        landmarksOfAllFaces.append(landmarks)

    return landmarksOfAllFaces

def draw(image, landmarks):
    ###########################################################################
    #
    #
    ###########################################################################

    assert image is not None, "[displayLandmarksByData]: image is NONE"
    assert landmarks is not None, "[displayLandmarksByData]: landmarks are NONE"

    display_image = image.copy()
    for index in range (0, len(landmarks)):
        pos = landmarks[index]
    
        cv2.circle(display_image,  (int(pos[0]),  int(pos[1])) ,  2, (0, 0, 255), 1)

    return display_image
