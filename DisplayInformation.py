import numpy as np
import cv2
import json
from pprint import pprint
import os, glob
import argparse
from facedetection import landmarkDetector as ld
import dlib

# get argument in
argParser = argparse.ArgumentParser()
argParser.add_argument("-f", "--folder", required = True,
    help = "path to image folder")

ag = argParser.parse_args()

for subfolder in os.listdir(ag.folder):
    subfolderPath = os.path.join(ag.folder, subfolder)

    if not os.path.isdir(subfolderPath):
        continue

    fileLookupString = str(subfolderPath) + "/*.json"

    for file in glob.glob(fileLookupString):
        imageName, extension = os.path.splitext(file)
        image = cv2.imread(imageName)
        with open(file) as dataFile: data = json.load(dataFile)

        landmarks = []
        for datum in data["landmarks"]:
            x = int(data["landmarks"][datum]["x"])
            y = int(data["landmarks"][datum]["y"])

            landmarks.append((x, y))
            cv2.circle(image, (x, y), 2, (0, 255, 0), 2)

        detector, predictor = ld.initiateDetector()
        image = ld.displayLandmarks(image, detector, predictor)
        cv2.imshow("image", image)
        cv2.waitKey()



#def autoCrop():
    ###########################################################################
    #
    # Create facial features by cropping face images
    #
    ###########################################################################
