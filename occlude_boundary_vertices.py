import numpy as np
import normalize_face as nf
import visible_vertices as vv

def occlude_boundary_vertices(FV, Ef, Ev, R = None):
    '''
    Function will return vertices in FV that lie on boundary edges

    Parameters:
        FV - dict type: contains 2 fields -- 'vertices' array of doubles
                                          -- 'faces' array of int

        Ef - array of doubles: faces adjacent to each edges in the mesh

        Ev - array of doubles: vertices adjacent to each face in the mesh

        R - 3x3 ndarray: rotation matrix. Default is None
    
    Return:
        occluding_vertices: 

    NOTE: If you are compared the result produced by this function with the one
          from matlab, keep in mind that 
            - the index of python is [0,...., n] - excluding n
            - the index of matlab is (1, ..., n) - including n
    '''

    if R is not None:
        FV['vertices'] = R.dot(FV['vertices'].T).T
    
    ## compute face normals
    fn = nf.normalize_face(FV['vertices'], FV['faces'])
    
    ## Edges with a zero index lie on the mesh boundary; They are only
    ## adjacent to one face
    #print(Ef)
    boundary_edges = (Ef[:, 0] == -1)
    #print(boundary_edges)
    
    idx1 = Ef[:, 0]
    idx1[boundary_edges] = 0

    ## compute the occluding edges as those where the two adjacent face normals
    ## differ in the sign of Z component
    
    occluding_edges = np.zeros(boundary_edges.shape, dtype = 'bool')
    a = np.sign(fn[idx1, 2]).astype(int)
    b = np.sign(fn[Ef[:, 1], 2]).astype(int)

    for i in range(len(boundary_edges)):
        occluding_edges[i] = (a[i] != b[i]) and (not boundary_edges[i])

    ## select the vertices lying at the end of the occluding edges and remove the duplicates
    occluding_vertices = Ev[occluding_edges, :]
    
    occluding_vertices = np.unique(occluding_vertices[:])
    
    visible_vertices = vv.get_visible_vertices(FV, np.identity(3))
    occluding_vertices = np.intersect1d(occluding_vertices, visible_vertices)

    return occluding_vertices


# import scipy.io as sio
# test_data = sio.loadmat('occludingBoundaryVertices/occludingVertices_data.mat')
# FV = test_data['FV']
# Ef = test_data['Ef'] - 1
# Ev = test_data['Ev'] - 1
# R = test_data['R']
#print(FV)
#print(Ef)
#print(Ev)
#print(R)
## redefine FV as dict, contains 2 keys: "vertices" and "faces"
# new_FV = dict()
## -1 is to adjust matlab index into python index
# new_FV['faces'] = FV[0,0][0] - 1
# new_FV['vertices'] = FV[0,0][1]
# occluding_vertices = occlude_boundary_vertices(new_FV, Ef, Ev, R)
# print(occluding_vertices)