import numpy as np
import math
import cv2

def render_face_fv(FV, T, oglp, background):
    # width of image plane
    width = oglp['width']

    ## height of image plane
    height = oglp['height']

    ## ambient light intensity
    i_amb_light = oglp['i_amb_light']

    ## directed light intensity
    i_dir_light = oglp['i_dir_light']

    ## directed light direction
    d_dir_light = np.array([0, 0, 1])
    
    shineness = 1
    
    specularity = 0
    
    ## flag for casting shadows
    do_cast_shadows = False

    ## get the extrinsic transformation matrix
    ## the output does not need to be in homogeneous coordinates
    M = T[0 : 3, :]

    ## get the vertices and number of vertices
    V = FV['vertices']
    n_vertices = V.shape[0]

    ## compute the transformed vertices
    V = np.append(V, np.ones([V.shape[0], 1]), 1)
    transformed_V = V.dot(M.T)

    ## store the vertex depths for z-buffering
    Z = transformed_V[:, 2]

    ## compute the projected vertices in the image plane
    UV = np.zeros([transformed_V.shape[0], 2])
    ## x orthographic projection
    UV[:, 0] = transformed_V[:, 0]
    ## y orthographic projection
    UV[:, 1] = transformed_V[:, 1]

    # Transform to the pixel plane (the axes remain switched)
    #   UV(:, 1)    = width - UV(:, 1);
    #   UV          = UV + 0.5;

    ## get the traingle vertices
    faces = FV['faces']
    v1 = faces[:, 0]
    v2 = faces[:, 1]
    v3 = faces[:, 2]
    num_of_faces = faces.shape[0]

    ## compute bounding boxes for the projected triangles
    x = np.zeros([len(v1), 3])
    x[:, 0] = UV[v1, 0]
    x[:, 1] = UV[v2, 0]
    x[:, 2] = UV[v3, 0]

    y = np.zeros([len(v1), 3])
    y[:, 0] = UV[v1, 1]
    y[:, 1] = UV[v2, 1]
    y[:, 2] = UV[v3, 1]
    
    minx = np.ceil(np.amin(x, axis = 1))
    maxx = np.floor(np.max(x, axis = 1))
    miny = np.ceil(np.min(y, axis = 1))
    maxy = np.floor(np.max(y, axis = 1))

    ## frustum culling ???
    minx = np.maximum([1], minx).astype(int)
    maxx = np.minimum([width], maxx).astype(int)
    miny = np.maximum([1], miny).astype(int)
    maxy = np.minimum([height], maxy).astype(int)
 
    ## construct the pixel grid -- can recompute if shared among images
    x_space = np.linspace(0, width - 1, width, dtype = 'int')
    y_space = np.linspace(0, height - 1, height, dtype = 'int')
    rows, cols = np.meshgrid(x_space, y_space)
    
    ## create buffers for: depth(z), face(f), and weights
    z_buffer = np.full([height, width], -np.inf)
    f_buffer = np.zeros([height, width])
    w1_buffer = np.full([height, width], np.nan)
    w2_buffer = np.full([height, width], np.nan)
    w3_buffer = np.full([height, width], np.nan)

    ## for each triangle (can speed up by comparing the triangle depths to the z-buffer
    ## and priorly sorting the triangles by increasing depths)
    i = 1
    for i in range(num_of_faces):
        ## if some pixels lie in the bounding box
        if minx[i] <= maxx[i] and miny[i] <= maxy[i]:
            ## get the pixels lying in bounding box
            ## Note that + 1 here is to adjust indexing from matlab to python
            px = rows[miny[i] : maxy[i] + 1, minx[i] : maxx[i] + 1]
            py = cols[miny[i] : maxy[i] + 1, minx[i] : maxx[i] + 1]
            
            e0 = UV[v1[i], :]
            e1 = UV[v2[i], :] - e0
            e2 = UV[v3[i], :] - e0

            ## compute the barycentric coordinates (can speed up by first computing and testing a sorely)
            det = e1[0] * e2[1] - e1[1] * e2[0]

            tmpx = px - e0[0]
            tmpy = py - e0[1]
            a = (tmpx * e2[1] - tmpy * e2[0]) / det
            b = (tmpy * e1[0] - tmpx * e1[1]) / det

            ## NOTE: there should be an easier way to write this
            test = np.ones(a.shape, dtype = 'bool')
            for k in range(a.shape[0]):
                for l in range(a.shape[1]):
                    test[k, l] = (a[k, l] >= 0 and b[k, l] >= 0 and (a[k, l] + b[k, l]) <= 1)

            if test.any():
                ## get the pixels on inside the triangle
                ## NOTE: - 1 here is to adjust matlab index to python index
                ## take out -1 if there is an issue with indexing
                px = px[test] - 1
                py = py[test] - 1

                ## interpolate the triangle for each pixel
                w2 = a[test]
                w3 = b[test]
                w1 = 1 - w2 - w3
               
                pz = Z[v1[i]] * w1 + Z[v2[i]] * w2 + Z[v3[i]] * w3

                for j in range(len(pz)):
                    if pz[j] > z_buffer[py[j], px[j]]:
                        z_buffer[py[j], px[j]] = pz[j]
                        f_buffer[py[j], px[j]] = i
                        w1_buffer[py[j], px[j]] = w1[j]
                        w2_buffer[py[j], px[j]] = w2[j]
                        w3_buffer[py[j], px[j]] = w3[j]

    ## NOTE: and again, there should be an easy way to handle this
    test = np.ones(f_buffer.shape, dtype = 'bool')
    for i in range(f_buffer.shape[0]):
        for j in range(f_buffer.shape[1]):
            test[i, j] = (f_buffer[i, j] != 0)

    f = np.unique(f_buffer[test]).astype(int)
    vv = np.zeros([len(f), 3])
    vv[:, 0] = v1[f]
    vv[:, 1] = v2[f]
    vv[:, 2] = v3[f]
    v = np.unique(vv)

    a1  = np.in1d(faces, v)
    a1 = np.reshape(a1, [int(len(a1) / 3), 3])
 
    a2 = np.any(a1, 1)
    f = np.nonzero(a2)
    f = f[0]
    # print((f))
    n_faces = len(f)
    # f = np.array(f)

    ## compute the edge vertices
    e1s = transformed_V[v2[f], :] - transformed_V[v1[f], :]
    e2s = transformed_V[v3[f], :] - transformed_V[v1[f], :]
    e3s = transformed_V[v2[f], :] - transformed_V[v3[f], :]

    # e1s = e1s[0, : , :]
    # e2s = e2s[0, : , :]
    # e3s = e3s[0, : , :]

    ## replicate fn_norm into 3 columns
    e1s_norm = normalize_edge(e1s)
    e2s_norm = normalize_edge(e2s)
    e3s_norm = normalize_edge(e3s)
    
    # e1s_norm = e1s_norm[0:10, :]
    # e2s_norm = e2s_norm[0:10, :]
    # e3s_norm = e3s_norm[0:10, :] 

    ## compute the angles
    angles = np.zeros([len(e1s_norm), 3])
    angles[:, 0] = np.arccos(np.sum(e1s_norm * e2s_norm, 1))
    angles[:, 1] = np.arccos(np.sum(e3s_norm * e1s_norm, 1))
    angles[:, 2] = math.pi - (angles[:, 0] + angles[:, 1])

    # Compute the triangle weighted normals
    triangle_normals = np.cross(e1s, e3s)
    w1_triangle_normals = normalize_triangle_weight(triangle_normals, angles[:, 0])
    w2_triangle_normals = normalize_triangle_weight(triangle_normals, angles[:, 1])
    w3_triangle_normals = normalize_triangle_weight(triangle_normals, angles[:, 2])

    # initiate vertex normals and update
    vertex_normals = np.zeros([n_vertices, 3])
    for i in range(n_faces):
        vertex_normals[v1[f[i]], : ] = vertex_normals[v1[f[i]], : ] + w1_triangle_normals[i, : ]
        vertex_normals[v2[f[i]], : ] = vertex_normals[v2[f[i]], : ] + w2_triangle_normals[i, : ]
        vertex_normals[v3[f[i]], : ] = vertex_normals[v3[f[i]], : ] + w3_triangle_normals[i, : ]

    # normalize vertex normals
    vertex_normals = normalize_edge(vertex_normals)

    # # prepare textures
    # texture_1 = np.zeros([n_vertices, 1])
    # texture_2 = np.zeros([n_vertices, 1])
    # texture_3 = np.zeros([n_vertices, 1])

    # H = [0; 0; 1] + d_dir_light;
    # H = H / norm(H, 2);

    # compute the dot products between the vertex normals and the directed light source
    N_dot_L = d_dir_light[0] * vertex_normals[:, 0] + d_dir_light[1] * vertex_normals[:, 1] + d_dir_light[2] * vertex_normals[:, 2]

    # get light diffusion
    # diffuse = np.nanmax(vertex_normals[:, 2])
    texture_1 = i_dir_light[0] * N_dot_L
    texture_2 = i_dir_light[1] * N_dot_L
    texture_3 = i_dir_light[2] * N_dot_L

    # initiate the image
    im1 = np.zeros([height, width])
    im2 = np.zeros([height, width])
    im3 = np.zeros([height, width])
    f_buffer = f_buffer.astype(int)
    # rasterize the image
    # v1 = v1 + 1
    indices = f_buffer.T[test.T]
    v1 = v1[indices]
    v2 = v2[indices]
    v3 = v3[indices]

    w1 = w1_buffer.T[test.T]
    w2 = w2_buffer.T[test.T]
    w3 = w3_buffer.T[test.T]

    im1.T[test.T] = w1 * texture_1[v1] + w2 * texture_1[v2] + w3 * texture_1[v3]
    im2.T[test.T] = w1 * texture_2[v1] + w2 * texture_2[v2] + w3 * texture_2[v3] 
    im3.T[test.T] = w1 * texture_3[v1] + w2 * texture_3[v2] + w3 * texture_3[v3] 

    ret_im = np.empty([im1.shape[0], im1.shape[1], 3])
    ret_im[:, : , 0] = im1
    ret_im[:, : , 1] = im2
    ret_im[:, : , 2] = im3

    # rows in reverse order
    ret_im = ret_im[ ::-1,:]

    # use this matrix to merge model with background
    test_3d = np.empty([test.shape[0], test.shape[1], 3])
    test_3d[ : , : , 0] = test[ ::-1,:]
    test_3d[ : , : , 1] = test[ ::-1,:]
    test_3d[ : , : , 2] = test[ ::-1,:]
    
    # normalize to pixel value before merging
    ret_im = normalize_matrix(ret_im)
    fit_model_no_bg = ret_im.copy()

    # background = normalize_matrix(background)

    # merge model with background
    h, w, d = ret_im.shape
    for i in range(h):
        for j in range(w):
            for k in range(d):
                if test_3d[i, j, k] == 0:
                    ret_im[i, j, k] = background[i, j, k]

    # ret_im[:, :, 0] = normalize_matrix(ret_im[:, :, 0])
    # ret_im[:, :, 1] = normalize_matrix(ret_im[:, :, 1])
    # ret_im[:, :, 2] = normalize_matrix(ret_im[:, :, 2])
    return ret_im, fit_model_no_bg


def normalize_matrix(array):
    maxVal = np.amax(array)
    minVal = np.amin(array)

    for r_i in range(0, len(array)):
        for c_j in range (0, len(array[0])):
            array[r_i, c_j] = ((array[r_i, c_j] - minVal) * (255) / (maxVal - minVal))
    return np.uint8(array)

def normalize_edge(edge): 
    edge_norm = np.sqrt(np.sum(edge ** 2, 1))
    edge_norm_repmat = np.reshape(np.tile(edge_norm, [1, 3]), [len(edge_norm), 3], order = 'F')
    return edge / edge_norm_repmat

def normalize_triangle_weight(triangle_normals, angles):
    angle_repmat = np.reshape(np.tile(angles, [1, 3]), [len(angles), 3], order = 'F')
    return triangle_normals * angle_repmat

# import scipy.io as sio
# test_data = sio.loadmat('render_face/render_face_FV_data.mat')
# FV_m = test_data['FV']
# T = test_data['T']
# oglp_m = test_data['oglp'] 
# background = test_data['background']

# ## redefine FV as dict, contains 2 keys: "vertices" and "faces"
# FV = dict()
# # -1 is to adjust matlab index into python index
# FV['vertices'] = FV_m[0,0][0]
# FV['faces'] = FV_m[0,0][1] - 1
# oglp = dict()
# oglp['height'] = oglp_m[0][0][0][0][0]
# oglp['width'] = oglp_m[0][0][1][0][0]
# oglp['i_amb_light'] = oglp_m[0][0][2][0]
# oglp['i_dir_light'] = oglp_m[0][0][3][0]

# im = render_face_fv(FV, T, oglp, background)
# cv2.imshow('fit', im)
# cv2.waitKey()