# Create the data.
import numpy as np
from numpy import pi, sin, cos, mgrid
import cv2
import scipy.io as sio
from tvtk.api import tvtk
from mayavi import mlab


# dphi, dtheta = pi/250.0, pi/250.0
# [phi,theta] = mgrid[0:pi+dphi*1.5:dphi,0:2*pi+dtheta*1.5:dtheta]
# m0 = 4; m1 = 3; m2 = 2; m3 = 3; m4 = 6; m5 = 2; m6 = 6; m7 = 4;
# r = sin(m0*phi)**m1 + cos(m2*phi)**m3 + sin(m4*theta)**m5 + cos(m6*theta)**m7
# x = r*sin(phi)*cos(theta)
# y = r*cos(phi)
# z = r*sin(phi)*sin(theta)
# print(x.dtype)
# im = cv2.imread('test1.jpg')
# x = np.float64(im[:, :, 0])
# y = np.float64(im[:, :, 1])
# z = np.float64(im[:, :, 2])
# print(x.shape)
# print(y.shape)
# print(z.shape)
# print(x.dtype)



def normalize_matrix(array):
    maxVal = np.amax(array)
    minVal = np.amin(array)

    for r_i in range(0, len(array)):
            array[r_i] = ((array[r_i] - minVal) * (255) / (maxVal - minVal))
    return np.float64(array)

def visual_3d_plot(x, y, z):
    pts = mlab.plot3d(x,y,z)
    mlab.show()

def visual_triangular_mesh(x, y, z, triList):
    pts = mlab.triangular_mesh(x, y, z, triList, color = (1,1,1))
    mlab.show()

def visual_3d_points_texture(x,y,z, texture):
    pts = mlab.points3d(x, y, z)
    sc = tvtk.UnsignedCharArray()
    sc.from_array(texture)

    pts.mlab_source.dataset.point_data.scalars = sc
    pts.mlab_source.dataset.modified()
    mlab.show()

def visual_mean_model():
    morphable_model = sio.loadmat('BFM/01_MorphableModel.mat')
 
    BFM_edges = sio.loadmat('BFMedgestruct.mat')

    triList = morphable_model['tl'] - 1

    shapeMU = morphable_model['shapeMU']
    texMU = morphable_model['texMU']

    shapeMU = np.reshape(shapeMU, [int(len(shapeMU) / 3), 3])
# # print(shapeMU)
    x = shapeMU[:, 0]
    y = shapeMU[:, 1]
    z = shapeMU[:, 2]

    pts = mlab.triangular_mesh(x, y, z, triList, color = (1,1,1))
    mlab.show()

def visual_predicted_shape(vertices, triList, texture=None):
    v = mlab.figure()

    mesh = tvtk.PolyData()
    mesh.points = vertices
    mesh.polys = triList

    p = tvtk.Property(color=(1, 1, 1))
    mapper = tvtk.PolyDataMapper()
    
    if texture is not None:
        mesh.point_data.scalars = texture.astype(np.uint8)
        
    mapper.set_input_data(mesh)

    actor = tvtk.Actor(mapper=mapper, property=p)    
    v.scene.add_actor(actor)
    mlab.show()

def visual_mean_model_with_texture():
    morphable_model = sio.loadmat('BFM/01_MorphableModel.mat')
 
    BFM_edges = sio.loadmat('BFMedgestruct.mat')

    triList = morphable_model['tl'] - 1

    shapeMU = morphable_model['shapeMU']
    texMU = morphable_model['texMU']
    shapeMU = np.reshape(shapeMU, [int(len(shapeMU) / 3), 3])
    texMU = np.reshape(texMU, [int(len(texMU) / 3), 3])

    v = mlab.figure()

    mesh = tvtk.PolyData()
    mesh.points = shapeMU
    mesh.polys = triList

    p = tvtk.Property(color=(1, 1, 1))
    mesh.point_data.scalars = texMU.astype(np.uint8)
    mapper = tvtk.PolyDataMapper()
    mapper.set_input_data(mesh)
    actor = tvtk.Actor(mapper=mapper, property=p)
    v.scene.add_actor(actor)
    mlab.show()
