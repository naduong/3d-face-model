import dlib
import face_detector as fd
import cv2
import time
import numpy as np
import fit_single_sop as fss
import face_rendering as fr
import fit_edge as fe
import helper
import optimize_hard_edge_cost as ohec
import os, imghdr
import argparse
import h5py
import csv

def normalize_matrix(array):
    maxVal = np.amax(array)
    minVal = np.amin(array)

    for r_i in range(0, len(array)):
            array[r_i] = ((array[r_i] - minVal) * (255) / (maxVal - minVal))
    return np.float64(array)

def get_3dlandmarks(csv_filename):
    """
    Read landmark data from csv file
    """
    landmarks = list()
    with open(csv_filename, newline='') as csvfile:
        landmark_data = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in landmark_data:
            landmarks.append(row)

    return np.asarray(landmarks)

def convert_2_BFM(landmarks, image):
    '''
    Convert detected landmarks into references into BFM and 2D positions
    in the format used within this implementation. 
    '''
    
    # source for indices: https://www.pyimagesearch.com/2017/04/03/facial-landmarks-dlib-opencv-python/
    converted_index_table = [i for i in range(17, 68)]

    # get from matlab 
    landmark_id = np.array ([
        26544,3099,4904,6065,6692,
        9285,9987,11014,12176,14110,
        8127,8138,8144,8150,6506,7336,8165,8989,10054,
        3117,4148,4923,5830,4675,3902,
        10651,11161,11805,12838,12075,11430,
        4878,5771,7238,8185,9127,10461,11245,9618,8675,8204,7731,6905,6165,7364,8191,9017,10087,8783,8192,7366,
        47849,43150,52739
    ])
    
    total_points = len(landmark_id)
    landmark_id = np.reshape(landmark_id, [total_points, 1])
    n_landmarks = np.zeros([total_points, 2])

    for i in range(len(converted_index_table)):   
        n_landmarks[i, :] = landmarks[converted_index_table[i], :]

    n_landmarks[51] = landmarks[8]
    n_landmarks[52] = landmarks[4]
    n_landmarks[53] = landmarks[12]
    n_landmarks[:, 1] = image.shape[0] + 1 - n_landmarks[:, 1]

    return n_landmarks.T, np.uint16(landmark_id)


def get_transform_model(shapePC, shapeMU, b, n_dims):
    a = shapePC[:, 0 : n_dims]
    vertices = a.dot(b) + shapeMU
    return np.reshape(vertices, [3, int(shapePC.shape[0] / 3)], order = 'F').T


def save_2d_image(FV, image, R, t, s, savePath, name, extension):
    rendered_image, model = fr.render_face(FV, image, R, t, s, False)
    dis = np.hstack((rendered_image, model))
    dis = np.hstack((image, rendered_image))

    save_path = os.path.join(savePath, name + '_processed' + extension)

    cv2.imwrite(save_path, dis)
    cv2.imshow('model', dis)
    cv2.waitKey()   


def fit(imPath, savePath, imName):
    name, extension = os.path.splitext(imName)
    
    im_path = os.path.join(imPath, imName)

    image = cv2.imread(im_path)
    if (image is None):
        print('Cannot read image ', im_path)
        return None

    # number of model dimensions to use
    n_dims = 14

    # Prior weight for initial landmark fitting
    w_intial_prior = 0.7

    # number iterations for iterative closest fitting
    i_c_f_iter = 7

    ## Initiate landmark predictor and reuse them as global objects
    ## detector is for detecting face
    ## Use face detection to detect landmarks in all faces available
    ## in that image
    start_time = time.time()
    ## predictor is for landmark prediction
    detector, predictor = fd.initiateDetector()


    # Note: landmarks of all faces in images will be detected.
    # For now, just get the first landmarks set
    landmarks = fd.detectFaces(image, detector, predictor)
    if len(landmarks) == 0:
        print ('CANNOT FIND A FACE IN IMAGE...')
    landmarks = landmarks[0]

    detecting_time = time.time() - start_time
    print('\nLandmark detection time = ' , detecting_time)

    # n_landmarks is landmark set of face in image
    # landmark_index is landmark reference from model
    n_landmarks, landmark_index = convert_2_BFM(landmarks, image)

    '''
    Initialize fitting using only landmarks
    '''
    b, R, t, s = fss.fit_single_sop(n_landmarks, shapePC, shapeMU, shapeEV, 
        n_dims, landmark_index, w_intial_prior)
    FV = dict()
    FV['vertices'] = get_transform_model(shapePC, shapeMU, b, n_dims)
    FV['faces'] = tl.T
    
    save_2d_image(FV, image, R, t, s, savePath, name + str(n_dims), extension)


## read model data from file
filename = 'BFM/model2017-1_bfm_nomouth.h5'
f = h5py.File(filename, 'r')

## mean shape
shapeMU = np.float64(f['/shape/model/mean'][:])
shapeMU = np.reshape(shapeMU, [len(shapeMU), 1])

## pca component of shape
shapePC = np.float64(f['/shape/model/pcaBasis'][:])

## pca variance
shapeEV = np.float64(f['/shape/model/pcaVariance'][:])
shapeEV = np.reshape(shapeEV, [len(shapeEV), 1])

## polygon mesh representer
tl = f['/shape/representer/cells'][:]

# get argument in
argParser = argparse.ArgumentParser()
argParser.add_argument("-i", "--input", help = "input can be image file or image folder")
ag = argParser.parse_args()

if os.path.isfile(ag.input):
    fit('', '', ag.input)

elif os.path.isdir(ag.input):
    newFolderName = 'fitted'
    ## prepare new folder path to save face
    newFolderPath = os.path.join(ag.input, newFolderName)
    if not os.path.exists(newFolderPath):
        os.makedirs(newFolderPath)

    for imName in os.listdir(ag.input):
        fit(ag.input, newFolderPath, imName)

else:
    print ('Invalid input argument.')