import numpy as np
from sklearn.neighbors import NearestNeighbors as knn
import cv2
import occlude_boundary_vertices as obv


'''
This function is borrowed from 
https://www.pyimagesearch.com/2015/04/06/zero-parameter-automatic-canny-edge-detection-with-python-and-opencv/
'''
def auto_canny(image, sigma=0.33):
	# compute the median of the single channel pixel intensities
	v = np.median(image)
 
	# apply automatic Canny edge detection using the computed median
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper)
 
	# return the edged image
	return edged

def get_edges_info(image):
    ## get the image edges
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    edges = auto_canny(gray_image)
    ## NOTE: use edges.T to make the output similar to matlab's. Will improve later
    c, r = np.nonzero(edges.T)
    
    ## Again, -1 two times is to adjust python index from matlab index
    r = edges.shape[0] + 1 - r - 1 - 1

    return c, r


def get_occluding_vertices(FV, Ef, Ev, vertices, R, t, s, r, c, percentile = 1):
    '''
    Get the set of occluding boundary vertices (those are lying on a mesh
    whose adjacent faces have a change of visibility)

    Parameters:
        FV: dict() object contains 'faces' and 'vertices'

        Ef: n_edges x 2 matrix storing faces adjacent to each edge

        Ev: n_edges x 2 matrix storing vertices adjacent to each edge

        R: Rotation matrix

        t: translation vector

        s: scale factor

        r, c: (canny) edges information

        Percentage: Propotion of nearest neighbor edge matches used at each
                    iteration, may be sensible to threshold on distance to nn
    '''

    ## Compute vertices lying on occluding boundary
    occluding_vertices = obv.occlude_boundary_vertices(FV, Ef, Ev, R)

    ## Project occluding boundary vertices using R, t, s
    bou_vertices = vertices[occluding_vertices, :].T
    x2 = np.dot(R, bou_vertices)
    x2 = x2[0 : 2, :]

    x2[0, :] = x2[0, :] + t[0]
    x2[1, :] = x2[1, :] + t[1]
    x2 = s * x2

    ## NOTE that the result from knn here is slightly different from
    ## result in matlab because of r and c. if using r + 1 and c + 1,
    ## result should be similar
    ## X = np.reshape(np.append(c + 1, r + 1), [len(c), 2], order = 'F')
    X = np.reshape(np.append(c, r), [len(c), 2], order = 'F')

    knbrs = knn(n_neighbors = 1, algorithm = 'kd_tree').fit(X)
    distances, indices = knbrs.kneighbors(x2.T)

    sorted_distance = np.sort(distances, axis = None)
    a = np.round(percentile * len(sorted_distance))
    threshold = sorted_distance[int(np.round(percentile * len(sorted_distance)) - 1)]
    idx = indices[distances < threshold]
    
    return occluding_vertices[distances[:, 0] < threshold], idx