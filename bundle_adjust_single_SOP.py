import numpy as np
import matrix_operation as mo
from scipy.optimize import least_squares

## NOTE: R, t, s from python implementation provides result similar to matlab, some small different in tail digits
##       b vector has some differences.
##       Need to revisit on this.

def bundle_adjust_single_SOP(x, b0, R0, t0, s0, shapePC, shapeMU, shapeEV, numsd, w1, w2 = None, nlandmarks = None):
    b_0 = np.append(mo.R_t_s_2_vec(R0, t0, s0), b0)
    
    ## define lower bound
    LB = np.empty((1, 5))
    LB[:] = -np.inf
    LB = np.append(LB, 0)
    LB = np.append(LB, -(numsd * shapeEV))
    
    ## define upper bound
    UB = np.empty((1, 6))
    UB[:] = np.inf
    UB = np.append(UB, numsd * shapeEV)

    if w2 is None and nlandmarks is None:
        solution = least_squares(bundle_adjust_nonlin_err_func, 
                                 b_0, 
                                 bounds = (LB, UB), 
                                 ftol=1e-06, 
                                 xtol=1e-06, 
                                 args = (x, shapePC, shapeMU, shapeEV, w1))
    else:
        solution = least_squares(bundle_adjust_nonlin_err_func, 
                                 b_0, 
                                 bounds = (LB, UB), 
                                 ftol=1e-06, 
                                 xtol=1e-06, 
                                 args = (x, shapePC, shapeMU, shapeEV, w1, w2, nlandmarks))

    b = solution['x']
    R, t, s = mo.vec_2_R_t_s(b[0 : 6])
    b_ret = b[6 : ]

    return b_ret, R, t, s
    

def bundle_adjust_nonlin_err_func(b, x, shapePC, shapeMU, shapeEV, w1, w2 = None, nlandmarks = None):
    '''
        x: landmark set -- 53 2d points
    '''
    ## TODO: let's reshape b outside of optimization process in order to speed up
    b = np.reshape(b, [b.shape[0], 1])
    R, t, s = mo.vec_2_R_t_s(b[0 : 6])
    a = shapePC.dot(b[6 : ]) + shapeMU
    X = np.reshape(a, [int(shapePC.shape[0] / 3), 3]).T

    ## rotate according to R, then translate and scale respectively to t and s
    x2 = R.dot(X)
    x2[0, :] = s * (x2[0, :] + t[0])
    x2[1, :] = s * (x2[1, :] + t[1])

    if w2 is None and nlandmarks is None:
        w = w1
        w1 = (1 - w) / (2 * x2.shape[1])
        w2 = w / len(shapeEV)
        a = np.append(x[0, : ] - x2[0, : ], x[1, : ] - x2[1, : ])
        a1 = w1 * a
        c = b[6 : ] / shapeEV  
        d = w2 * c
        result = np.append(a1, d)
    else:
        w3 = 1 - w1 - w2
        n_edges = x.shape[1] - nlandmarks
        w1 = w1 / (2 * n_edges)
        w2 = w2 / (2 * nlandmarks)
        w3 = w3 / (len(shapeEV))  

        ## residuals_edges = w1.*[(x(1,1:nedges)-x2(1,1:nedges)) (x(2,1:nedges)-x2(2,1:nedges))];
        aa = x[0, 0 : n_edges] - x2[0, 0 : n_edges]
        bb = x[1, 0 : n_edges] - x2[1, 0 : n_edges]
        cc = np.append(aa, bb)
        residuals_edges = w1 * cc

        ## residuals_landmarks = w2.*[(x(1,nedges+1:end)-x2(1,nedges+1:end)) (x(2,nedges+1:end)-x2(2,nedges+1:end))];
        a1 = x[0, n_edges : ] - x2[0, n_edges : ]
        b1 = x[1, n_edges : ] - x2[1, n_edges : ]
        c1 = np.append(a1, b1)
        residuals_landmarks = w2 * c1

        ## residuals_prior = (w3.*(b(7:end)./(shapeEV)))';  
        a2 = b[6 : ] / shapeEV
        residuals_prior = w3 * a2

        result = np.append(np.append(residuals_edges, residuals_landmarks), 
                           residuals_prior)

    return result
        
# import scipy.io as sio
# import scipy.spatial.distance as dist
# test_data = sio.loadmat('bundleAdjustSingleSOP/BANonlinerrfun_data_3.mat')

# shapeEV = test_data['shapeEV']
# shapeMU = test_data['shapeMU']
# shapePC = test_data['shapePC']
# b = test_data['b']
# w1 = test_data['w1']
# #w2 = test_data['w2']
# #n_landmarks = test_data['nlandmarks']
# x = test_data['x']

# result = bundle_adjust_nonlin_err_func(b, x, shapePC, shapeMU, shapeEV, w1)
# test_result = sio.loadmat('bundleAdjustSingleSOP/BANonlinerrfun_result_3.mat')
# l2_dist = dist.euclidean(result, test_result['result'])
# # print(result)
# # print(test_result['result'])
# # print(l2_dist)

# test_data = sio.loadmat('bundleAdjustSingleSOP/BundleAdjustSingleSOP_data_2.mat')
# shapeEV = test_data['shapeEV']
# shapeMU = test_data['shapeMU']
# shapePC = test_data['shapePC']
# b0 = test_data['b0']
# R0 = test_data['R0']
# t0 = test_data['t0']
# s0 = test_data['s0']
# numsd = test_data['numsd']
# w1 = test_data['w1']
# w2 = test_data['w2']
# nlandmarks = int(test_data['nlandmarks'])
# x = test_data['x']
# #print(x)
# b,R, t, s = bundle_adjust_single_SOP(x, b0, R0, t0, s0, shapePC, shapeMU, shapeEV, numsd, w1, w2, nlandmarks)
# test_result2 = sio.loadmat('bundleAdjustSingleSOP/BundleAdjustSingleSOP_result_2.mat')
# # print(b)
# # print(test_result2['b'])
# l2_dist = dist.euclidean(b, test_result2['b'])
# print(l2_dist)